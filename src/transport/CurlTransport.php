<?php
namespace realgaming\api\client\transport;
use realgaming\api\client\Transport;

/**
 * Class CurlTransport
 * @package realgaming\api\client
 */
class CurlTransport extends Transport
{
	private $curlHandle;

	public function sendRequest($method, $data = array())
	{
		$handle = $this->getCurlHandle();

		curl_setopt($handle, CURLOPT_URL, $this->apiUrl.'/'.$method.($data ? '?'.http_build_query($data) : ''));

		$response = curl_exec($handle);

		if ($response === false) {
			$error = curl_error($handle);
			throw new \Exception($error);
		}

		return $response;
	}

	protected function getCurlHandle()
	{
		if ($this->curlHandle === null)
			$this->initCurlHandle();
		return $this->curlHandle;
	}

	protected function initCurlHandle()
	{
		$this->curlHandle = curl_init();
		curl_setopt_array($this->curlHandle, array(
			CURLOPT_RETURNTRANSFER => true,
		));
	}

	protected function resetConnection()
	{
		if($this->curlHandle !== null)
		{
			curl_close($this->curlHandle);
			$this->curlHandle = null;
		}
	}
}