<?php
namespace realgaming\api\client;
use realgaming\api\client\transport\CurlTransport;

/**
 * Class Client
 * @package realgaming\api\client
 */
class Client
{
	/** @var Transport */
	private $transport;

	public $token;
	public $secretKey;
	public $groupId;
	public $gamesUrl;

	/**
	 * @param array $connectionData
	 * @param Transport $transport
	 */
	public function __construct($connectionData, $transport = null)
	{
		$this->transport = $transport;
		$this->token = $connectionData['token'];
		$this->secretKey = $connectionData['secretKey'];
		$this->gamesUrl = $connectionData['gamesUrl'];
		$this->groupId = array_key_exists('groupId', $connectionData) ? $connectionData['groupId'] : null;
		unset($connectionData['token'], $connectionData['secretKey'], $connectionData['gamesUrl'], $connectionData['groupId']);
		$this->getTransport()->setConnectionData($connectionData);
	}

	public function listGames()
	{
		return array(
			array('id' => 1,        'string_id' => 'rky',       'name' => 'Rocky',                      'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 2,        'string_id' => 'irm3',      'name' => 'Iron Man 2',                 'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 3,        'string_id' => 'irm50',     'name' => 'Iron Man 2 50 Lines',        'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 4,        'string_id' => 'wvm',       'name' => 'Wolverine',                  'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 5,        'string_id' => 'irmn3',     'name' => 'Iron Man 3',                 'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 6,        'string_id' => 'trm',       'name' => 'Thor',                       'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 7,        'string_id' => 'avng',      'name' => 'Avengers',                   'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 9,        'string_id' => 'irm2',      'name' => 'Iron Man',                   'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 10,       'string_id' => 'fnf',       'name' => 'Fantastic',                  'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 11,       'string_id' => 'fnf50',     'name' => 'Fantastic 50 Lines',         'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 12,       'string_id' => 'hlk2',      'name' => 'Hulk',                       'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 13,       'string_id' => 'hlk50',     'name' => 'Hulk 50 Lines',              'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 14,       'string_id' => 'drd',       'name' => 'Dare Devil',                 'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 15,       'string_id' => 'elr',       'name' => 'Elektra',                    'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 16,       'string_id' => 'ghr',       'name' => 'Ghost Rider',                'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 17,       'string_id' => 'bld',       'name' => 'Blade',                      'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 18,       'string_id' => 'xmn',       'name' => 'X-Men',                      'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 19,       'string_id' => 'glr',       'name' => 'Gladiator',                  'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 20,       'string_id' => 'kkg',       'name' => 'Kong',                       'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 21,       'string_id' => 'bib',       'name' => 'Great Blue',                 'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 22,       'string_id' => 'gs',        'name' => 'Diamond Valley',             'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 23,       'string_id' => 'pnp',       'name' => 'Pink Panther',               'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 24,       'string_id' => 'fff',       'name' => 'Funky Fruits Farm',          'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 25,       'string_id' => 'lm',        'name' => 'Lotto Madness',              'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 26,       'string_id' => 'dlm',       'name' => 'Dr. Lovemore',               'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 27,       'string_id' => 'spm',       'name' => 'Spamalot',                   'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 28,       'string_id' => 'spidc',     'name' => 'Spider-Man',                 'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 29,       'string_id' => 'dt2',       'name' => 'Desert Treasure II',         'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 30,       'string_id' => 'hlf',       'name' => 'Halloween',                  'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 31,       'string_id' => 'xmn50',     'name' => 'X-Men 50 Lines',             'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 32,       'string_id' => 'mcb',       'name' => 'Cashback',                   'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 33,       'string_id' => 'paw',       'name' => 'Piggies And The Wolf',       'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 34,       'string_id' => 'ssp',       'name' => 'Santa Surprise',             'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 35,       'string_id' => 'fdt',       'name' => 'Frankie Dettoris',           'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 36,       'string_id' => 'bl',        'name' => 'Beach Life',                 'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 37,       'string_id' => 'ct',        'name' => 'Captains Treasure',          'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 38,       'string_id' => 'hb',        'name' => 'A Night Out',                'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 39,       'string_id' => 'wsffr',     'name' => 'Wall Street 4ever',          'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 40,       'string_id' => 'dt',        'name' => 'Desert Treasure',            'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 41,       'string_id' => 'glrj',      'name' => 'Gladiator Jackpot',          'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 42,       'string_id' => 'fm',        'name' => 'Funky Monkey',               'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 43,       'string_id' => '8bs',       'name' => '8-Ball Slot',                'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 44,       'string_id' => 'bt',        'name' => 'Bermuda Triangle',           'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 45,       'string_id' => 'c7',        'name' => 'Crazy 7',                    'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 46,       'string_id' => 'zcjb',      'name' => 'Zhao Cai Jin Bao',           'description' => '',    'path' => 'flash2/80/casino/'),
			array('id' => 47,       'string_id' => 'wlg',       'name' => 'Wu Long',                    'description' => '',    'path' => 'flash2/80/casino/'),
			array('id' => 48,       'string_id' => 'hk',        'name' => 'Highway Kings',              'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 49,       'string_id' => 'sc',        'name' => 'Safecracker',                'description' => '',    'path' => 'flash2/80/casino/'),
			array('id' => 50,       'string_id' => 'fnfrj',     'name' => 'Funky Fruits',               'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 51,       'string_id' => 'grel',      'name' => 'Gold Rally',                 'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 52,       'string_id' => 'er',        'name' => 'Vacation',                   'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 53,       'string_id' => 'whk',       'name' => 'White King',                 'description' => '',    'path' => 'flash2/80/casino/'),
			array('id' => 61,       'string_id' => 'gos',       'name' => 'Golden Tour',                'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 72,       'string_id' => 'po',        'name' => 'Jacks or Better',            'description' => '',    'path' => 'flash/47/casino/'),
			array('id' => 77,       'string_id' => 'rom',       'name' => 'Marvel Roulette',            'description' => '',    'path' => 'flash/47/casino/'),
		);
	}

	/**
	 * @param $currency
	 * @return string A new group id
	 * @throws Exception
	 */
	public function createGroup($currency)
	{
		$response = $this->callApiMethod('create_group', array(
			'currency' => $currency
		));

		return $response['group_id'];
	}

	/**
	 * @param string $player
	 * @param array $params
	 * @return string Token of game session to open games
	 * @throws Exception
	 */
	public function openGameSession($player, $params = array())
	{
		$params = array_merge(array(
			'user_id' => $player,
			'balance' => 0,
		), $params);

		$response = $this->callApiMethod('open_session', $params + array('group_id' => $this->groupId));

		return $response['key'];
	}

	/**
	 * @param string $stringId
	 * @param string $player
	 * @param array $params
	 * @throws Exception
	 * @return string Game link to open game
	 */
	public function runGame($stringId, $player, $params = array())
	{
		$game = $this->_findGame($stringId);

		if($game === null)
		{
			throw new Exception('Game not found');
		}

		$subParams = [];
		if(array_key_exists('group_id', $params))
			$subParams['group_id'] = $params['group_id'];

		try { $this->closeGameSession($player, $subParams); } catch(Exception $e) {}

		$sessionKey = $this->openGameSession($player, $subParams);

		return $this->gamesUrl.$game['path'].'?'.http_build_query(array('game' => $stringId, 'key' => $sessionKey, 'preferedmode' => 'real', 'language' => 'en'));
	}

	/**
	 * @param $stringId
	 * @return null|array
	 */
	private function _findGame($stringId)
	{
		$game = null;
		foreach($this->listGames() as $item)
		{
			if($item['string_id'] == $stringId)
			{
				$game = $item;
				break;
			}
		}

		return $game;
	}

	/**
	 * @param $player
	 * @param array $params
	 * @return string Returns balance in the end of game session
	 * @throws Exception
	 */
	public function closeGameSession($player, $params = array())
	{
		$params = array_merge(array(
			'user_id' => $player,
			'reset_balance' => 0,
		), $params);

		$response = $this->callApiMethod('close_session', $params + array('group_id' => $this->groupId));

		return $response['balance'];
	}

	/**
	 * @param $player
	 * @param array $params
	 * @return string Returns player balance
	 * @throws Exception
	 */
	public function getBalance($player, $params = array())
	{
		$params = array_merge(array(
			'user_id' => $player,
		), $params);

		$response = $this->callApiMethod('get_balance', $params + array('group_id' => $this->groupId));

		return $response['balance'];
	}

	/**
	 * @param $player
	 * @param $amount
	 * @param array $params
	 * @return string Returns player balance after operation
	 * @throws Exception
	 */
	public function changeBalance($player, $amount, $params = array())
	{
		$params = array_merge(array(
			'user_id' => $player,
			'amount' => $amount,
		), $params);

		$response = $this->callApiMethod('change_balance', $params + array('group_id' => $this->groupId));

		return $response['balance'];
	}

	/**
	 * @param $player
	 * @param array $params
	 * @return string Returns amount of withdrawn balance
	 * @throws Exception
	 */
	public function withdrawAllBalance($player, $params = array())
	{
		$params = array_merge(array(
			'user_id' => $player,
		), $params);

		$response = $this->callApiMethod('withdraw_balance', $params + array('group_id' => $this->groupId));

		return $response['balance'];
	}

	/**
	 * @param $player
	 * @param integer $start
	 * @param integer $end
	 * @param array $params
	 * @return array Returns array of statistic for player by start and end time
	 * @throws Exception
	 */
	public function getStatistic($player, $start, $end = null, $params = array())
	{
		$start = explode(' ', date('Y-m-d H i', $start));
		if($end === null)
			$end = time();

		$end = explode(' ', date('Y-m-d H i', $end));

		$params = array_merge(array(
			'user_id' => $player,
			'date_from' => $start[0],
			'h_from' => $start[1],
			'm_from' => $start[2],
			'date_to' => $end[0],
			'h_to' => $end[1],
			'm_to' => $end[2],
		), $params);

		$response = $this->callApiMethod('get_stat', $params + array('group_id' => $this->groupId));

		return array_key_exists('stat', $response) ? $response['stat'] : [];
	}

	/**
	 * @param integer $start
	 * @param integer|null $end
	 * @param array $params
	 * @return array
	 * @throws Exception
	 */
	public function getReportByUser($start, $end = null, $params = array())
	{
		$start = explode(' ', date('Y-m-d H i', $start));
		if($end === null)
			$end = time();

		$end = explode(' ', date('Y-m-d H i', $end));

		$params = array_merge(array(
			'date_from' => $start[0],
			'h_from' => $start[1],
			'm_from' => $start[2],
			'date_to' => $end[0],
			'h_to' => $end[1],
			'm_to' => $end[2],
		), $params);

		$response = $this->callApiMethod('get_report_by_user', $params + array('group_id' => $this->groupId));

		return array_key_exists('report', $response) ? $response['report'] : [];
	}

	protected function callApiMethod($method, $data = array())
	{
		$params = array(
			'token' => $this->token,
			'secret_key' => $this->secretKey,
			'format' => 'json'
		);

		$responseData = $this->getTransport()->sendRequest($method, $params + $data);

		$response = json_decode($responseData, true);

		if($response === null) {
			throw new Exception('Cannot read json');
		}

		if(array_key_exists('error', $response))
			throw new Exception($this->getErrorById($response['error']));

		return $response;
	}

	protected function getErrorById($errorId)
	{
		$errors = array(
			'1' => 'Wrong parameter or parameters',
			'2' => 'Wrong currency',
			'3' => 'Wrong token or secret key',
			'4' => 'wrong jackpot group id',
			'5' => 'wrong user id',
			'6' => 'no opened game session for this user',
			'7' => 'not enough money for operation',
			'8' => 'wrong dates',
		);

		if(!array_key_exists($errorId, $errors))
			return 'Unknown error #'.$errorId;

		return $errors[$errorId];
	}

	protected function getTransport()
	{
		if ($this->transport === null)
			$this->transport = new CurlTransport();

		return $this->transport;
	}
}
