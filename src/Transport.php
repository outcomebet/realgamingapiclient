<?php
namespace realgaming\api\client;

abstract class Transport
{
	protected $apiUrl;

	public function setConnectionData($connectionData)
	{
		$this->apiUrl = rtrim($connectionData['apiUrl'], '/');
	}

	protected function resetConnection()
	{
	}

	abstract public function sendRequest($method, $data = array());
}